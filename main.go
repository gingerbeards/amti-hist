package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/jyap808/go-poloniex"
)

var (
	TITLES = []string{"date_time", "currencyPair", "last", "lowestAsk", "highestBid", "percentChange",
		"baseVolume", "quoteVolume", "isFrozen", "24hrHigh", "24hrLow"}
	PAIR = "USDT_BTC"
)

type tickerData poloniex.Ticker

func (this tickerData) WriteCSV(pair string, w *csv.Writer) error {
	record := []string{
		time.Now().String(),
		pair,
		this.Last.String(),
		this.LowestAsk.String(),
		this.HighestBid.String(),
		this.PercentChange.String(),
		this.BaseVolume.String(),
		this.QuoteVolume.String(),
		strconv.Itoa(this.IsFrozen),
		this.High24Hr.String(),
		this.Low24Hr.String(),
	}

	err := w.Write(record)
	w.Flush()
	return err
}

func main() {
	// Poloniex client
	p := poloniex.New("", "")

	// File
	f, err := os.Create("ticker_data.csv")
	if err != nil {
		panic(err)
	}

	// Csv writer
	w := csv.NewWriter(f)
	err = w.Write(TITLES)
	if err != nil {
		panic(err)
	}

	// ticker
	ticker := time.NewTicker(time.Second)

	// Tick
	for {
		<-ticker.C

		err := runTicker(p, w)
		if err != nil {
			panic(err)
		}
	}
}

func runTicker(p *poloniex.Poloniex, w *csv.Writer) error {
	tickers, err := p.GetTickers()
	if err != nil {
		return err
	}

	ticker, exists := tickers[PAIR]
	if !exists {
		return fmt.Errorf("pair %v doesn't exist", PAIR)
	}

	return tickerData(ticker).WriteCSV(PAIR, w)
}
